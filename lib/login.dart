import 'package:flutter/material.dart';
import 'package:flutter_auth_buttons/flutter_auth_buttons.dart';

import 'package:velocity_x/velocity_x.dart';

final formKey = GlobalKey<FormState>();
bool autovalidate = false;

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Login Your Account').text.xl4.extraBold.center.make().pOnly(bottom: 30),
              Text('''
                Fill all fields so we can get some info about you.
                You will receive and SMS for verification
                ''').text.base.gray700.light.center.make().pOnly(bottom: 30),
              TextField(
                decoration: new InputDecoration(
                    border: new OutlineInputBorder(
                      borderSide: new BorderSide(
                        color: Vx.black,
                      ),
                    ),
                    hintText: 'Username'),
              ).pOnly(bottom: 30),
              TextField(
                decoration: new InputDecoration(
                    border: new OutlineInputBorder(
                      borderSide: new BorderSide(
                        color: Vx.black,
                      ),
                    ),
                    hintText: 'Password'),
                obscureText: true,
              ).pOnly(bottom: 30),
              SizedBox(
                child: FlatButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  onPressed: () {},
                  child: Text('Login'),
                ),
                width: double.infinity,
                height: 60,
              ).pOnly(bottom: 30),
              Align(
                alignment: Alignment.centerRight,
                child: GestureDetector(
                  child: Text('Forgot your Password?').text.underline.make(),
                  onTap: () {},
                ),
              ).pOnly(bottom: 60),
              FacebookSignInButton(
                onPressed: (){}
              ).pOnly(bottom: 30),
              GoogleSignInButton(
                onPressed: (){}
              )
              // SizedBox(
              //   child: FacebookSignInButton(
              //     onPressed: (){},
              //   ),
              //   width: double.infinity,
              //   height: 60,
              // ).pOnly(bottom: 30),
              // SizedBox(
              //   child: GoogleSignInButton(
              //     darkMode: true,
              //     onPressed: (){

              //     },
              //   ),
              //   width: double.infinity,
              //   height: 60,
              // )
            ],
          ).p(20),
        ),
      ),
    );
  }
}
